import json

def configRead(key) :

	with open("config.json") as file:
		config = json.load(file)
		value = config[key]
		return value
