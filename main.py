from readConfigFile import *
from Queue import *
from deviceRegistration import *
from BACnetApp import *
import queue
import paho.mqtt.client as mqtt
import threading
import http.client
from BACnetQueue import *
from httpClient import *
from macIdIdentification import *
import json

ANALOG_OUTPUT = 1
ANALOG_VALUE = 2

mparserToBACnetDataQueue = queue.Queue(0)
BACnetToMparserDataQueue = queue.Queue(0)
serverId = configRead("serverId")
configFileExistance()


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata,flags,rc):

    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("bacnet/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
	print("msg is  ",msg.topic)
	topics = msg.topic.split("/")
	print(topics)
	if topics[1] != "analogOutput" :
		mparserToBACnetDataQueue.put_nowait(msg)
		print("data from mparser")
	else :
		BACnetToMparserDataQueue.put_nowait(msg)
		print("data from BACnet")


def primary():

	client = mqtt.Client("001")
	client.on_connect = on_connect
	client.on_message = on_message
	client.connect("localhost", 1883, 60)
	client.loop_forever()


def mparserToBACnetQueueProcessing():
	print("thread count: ",threading.active_count())

	while 1:
		Flag, topic, BACnetData = getQueue(mparserToBACnetDataQueue)

		if Flag :
			objectType = objectTypeIdentification(topic)

			if objectType == ANALOG_VALUE :
				rdObjectId = analogValueDeviceExist(topic)
				print("rdobjectid main",rdObjectId)
			else :
				rdObjectId = analogOutputDeviceExist(topic)

			if len(rdObjectId) and objectType == ANALOG_VALUE :
				BACnetDataWrite(rdObjectId, BACnetData, serverId, objectType)
			elif not len(rdObjectId) and objectType == ANALOG_VALUE :
				objectNameList, objectList = deviceRegistartions(topic)
				BACnetObjectNameWrite(objectList, objectNameList, serverId, objectType)
				BACnetDataWrite(objectList, BACnetData, serverId, objectType)
			elif not len(rdObjectId) and objectType == ANALOG_OUTPUT :
				objectNameList, objectList = deviceRegistartions(topic)
				BACnetObjectNameWrite(objectList, objectNameList, serverId, objectType)
			else :
				pass
		else :
			pass


def BACnetToMparserQueueProcessing():

	while 1:
		Flag, topic, mparserData = getBACnetQueue(BACnetToMparserDataQueue)
#		print(type(mparserData))

		if Flag :

#			print(Flag)

			MACId = getMACId(topic)
			print(MACId)

			httpCommandApi(mparserData,MACId,4)

#			conn = http.client.HTTPConnection("localhost")

#			payloadDict = {"device":{"macAddress" : "5027000000000312"}, "command":{"id" : 3, "args":int(float(mparserData))}}
#			print(type(prepareJson))
#			payloadJson = json.dumps(prepareJson)

#			payload = "{\n\t\"device\":{\n\t  \"macAddress\": \"5027000000000312\"\n\t  },\n\t  \"command\": {\n\t   \"id\":3,\n\t   \"args\":50\n\t  }\n}"

#			headers = {
#			    'Content-Type': "application/json",
#			    'cache-control': "no-cache",
#			    'Postman-Token': "c03b914e-93bf-4dcf-99c7-3bdb6c2e17c1"
#			    }

#			conn.request("POST", "/api/command", payload, headers)

#			res = conn.getresponse()
#			data = res.read()

#			print(data.decode("utf-8"))

		else :
			pass


if __name__== "__main__":
	main_thread = threading.Thread(target=primary)
	queue_thread = threading.Thread(target=mparserToBACnetQueueProcessing)
	mparser_thread = threading.Thread(target=BACnetToMparserQueueProcessing)
	main_thread.start()
	queue_thread.start()
	mparser_thread.start()

