import http.client
import json

def httpCommandApi(args,MACId,cmdId):

	conn = http.client.HTTPConnection("localhost")

#	payloadDict = {"device":{"macAddress" : "5028000000000001"}, "command":{"id" : 4, "args":int(float(args))}}
	payloadDict = {"device":{"macAddress" : MACId}, "command":{"id" : cmdId, "args":int(float(args))}}
	print(type(payloadDict))
	payloadJson = json.dumps(payloadDict)

#	payload = "{\n\t\"device\":{\n\t  \"macAddress\": \"5027000000000312\"\n\t  },\n\t  \"command\": {\n\t   \"id\":3,\n\t   \"args\":50\n\t  }\n}"

	headers = {
	    'Content-Type': "application/json",
	    'cache-control': "no-cache",
	    'Postman-Token': "c03b914e-93bf-4dcf-99c7-3bdb6c2e17c1"
	    }

	conn.request("POST", "/api/command", payloadJson, headers)

	res = conn.getresponse()
	data = res.read()
	print(data.decode("utf-8"))
