import queue
import json
import collections

def getBACnetQueue(dataQueue) :
	#print("queue",dataQueue.empty())
	BACnetData = None
	topics = []
	if not dataQueue.empty():
		msg = dataQueue.get()
		topics = msg.topic.split("/")
		data = msg.payload.decode()
		BACnetData = data
		print("topic is:  ",topics)
		print("data from bacnet",BACnetData)
		return True, topics, BACnetData
	else:
		return False, topics,BACnetData
