import subprocess
import os
from time import *

BACnetExecutablePath = "/extsd/bacnet-stack-0.8.6_working/bin"
pythonExecutablePath = "/extsd/BACnet_bkp_http_bkp_working"

def BACnetObjectNameWrite(objectList, objectNameList, serverId, objectType) :
	""" Write the object name in BACnet stack"""

	print("write object name")
	for  objects,objectName in zip(objectList,objectNameList):
		os.chdir(BACnetExecutablePath)
		print("type : ",objectType)
		subprocess.call(["./bacwp",str(serverId),str(objectType),str(objects),"77","0","-1","7",str(objectName)])
		sleep(2)
	os.chdir(pythonExecutablePath)

def BACnetDataWrite(objectList, BACnetData, serverId, objectType):
	"""Write the present value of end device into the BACnet stack"""

	#currentDirectory = os.getcwd()
	#print(currentDirectory)
	#BACnetData = list(data.values())
	print("BACNET data",BACnetData)
	print("object list :",objectList)
	print("server bId",serverId)
	print("objectType",objectType)
	for (objects,presentValue) in zip(objectList,BACnetData) :
		print("BACnet Write")
		print(presentValue)
		os.chdir(BACnetExecutablePath)
		print(os.getcwd())
		print(objects)
		print(str(serverId))
		subprocess.call(["./bacwp",str(serverId),str(objectType), str(objects),"85","16", "-1", "4", str(presentValue)])
		sleep(2)
	os.chdir(pythonExecutablePath)
