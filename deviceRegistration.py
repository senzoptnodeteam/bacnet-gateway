import csv
import os

ANALOG_OUTPUT = 1
ANALOG_VALUE = 2
AVObjectId = 0
AOObjectId = 0

def configFileExistance() :
	if os.path.exists("config.csv") and os.path.exists("analogOutputRegistration.csv"):
		pass
	elif (not os.path.exists("config.csv")) and os.path.exists("analogOutputRegistration.csv"):
		createFile()
	elif os.path.exists("config.csv") and (not os.path.exists("analogOutputRegistration.csv")):
		createAnalogOutputFile()
	else :
		createFile()
		createAnalogOutputFile()

def deviceRegistartions(topic) :
	cmdId, objectType, numberOfObject, objectNameList = deviceTypeIdentification(topic)
	objectList = assignObjectId(topic, numberOfObject, objectNameList, objectType, cmdId)
	return  objectNameList, objectList

def analogValueDeviceExist(topic):
	""" Check device existance in the csv log """

	#f = open("config.csv","r")
	with open("config.csv", 'r') as f:
		print("AV read file")
		f.seek(0)
		reader = csv.reader(f)
		print("csv")
		rdObjectId = []
		print(rdObjectId)
		print("deviceexist")
#		fieldname = f.next()
		print(reader)
		for row in reader :
			print(reader)
			print(row)
			print("row length :",len(row))
			for MACId in row :
				print("MAC",MACId)
				print(len(MACId))
				if MACId == topic[2] :
					print("topic",topic[2])
					print("macid",MACId)
					rdObjectId.append(row[3])
					print("objectID:",rdObjectId)
				else:
					pass
		#print(rdObjectId)
		return rdObjectId


def analogOutputDeviceExist(topic):
	""" Check device existance in the csv log """

	#f = open("config.csv","r")
	with open("analogOutputRegistration.csv", 'r') as f:
		print("read file")
		f.seek(0)
		reader = csv.reader(f)
		print("csv")
		rdObjectId = []
		print(rdObjectId)
		print("deviceexist")
#		fieldname = f.next()
		print(reader)
		for row in reader :
			print(reader)
			print(row)
			print("row length :",len(row))
			for MACId in row :
				print(MACId)
				print(len(MACId))
				if MACId == topic[2] :
					print(topic[2])
					print(MACId)
					rdObjectId.append(row[3])
					print(rdObjectId)
				else:
					pass
		#print(rdObjectId)
		return rdObjectId


def createFile():
    """ Create CSV log file to log the device info"""

    f = open("config.csv", "a", newline='')
    writer = csv.DictWriter(
    f, fieldnames=["DeviceType", "Dev_MACID", "ObjectName","ObjectId"])
    writer.writeheader()
    print("header")
    f.close()

def createAnalogOutputFile():
    """ Create CSV log file to log the device info"""

    f = open("analogOutputRegistration.csv", "a", newline='')
    writer = csv.DictWriter(
    f, fieldnames=["DeviceType", "Dev_MACID", "ObjectName","ObjectId","commandId"])
    writer.writeheader()
    print("header")


def objectTypeIdentification(topic) :
	""" Identify object type for end device"""
	if(topic[1] == "THL"):
		objectType = ANALOG_VALUE
	elif (topic[1] == "WI_PIR") :
		objectType = ANALOG_VALUE
	elif (topic[1] == "VTC") :
		objectType = ANALOG_OUTPUT
	else :
		objectType = -1
	return objectType


def deviceTypeIdentification(topic):
    """ Identify the device type """
    cmdId = None
    objectNameList = list()

    if(topic[1] == "THL"):
        numberOfObject = 3
        objectNameList.append("Humi_" + topic[2][14:])
        objectNameList.append("Lux_" + topic[2][14:])
        objectNameList.append("Temp_" + topic[2][14:])
        objectType = ANALOG_VALUE
    elif (topic[1] == "WI_PIR") :
        numberOfObject = 1
        objectNameList.append("WI_PIR_" + topic[2][14:])
        objectType = ANALOG_VALUE
    elif (topic[1] == "VTC") :
        numberOfObject = 1
        objectNameList.append("VTC_" + topic[2][14:])
        objectType = ANALOG_OUTPUT
        cmdId = 4
    else :
        numberOfObject = 0
        objectType = -1
    print("device Identified")
    return cmdId,objectType,numberOfObject,objectNameList


def assignObjectId(topic,numberOfObject, objectNameList,objectType, cmdId):
	""" Assign object ID based on device type and number of objects"""

	global AVObjectId
	global AOObjectId
	objectList = []

	if objectType == ANALOG_VALUE :
		for x in range(numberOfObject) :
			AVObjectId = AVObjectId + 1
			print("assign id",AVObjectId)
			objectList.append(AVObjectId)
			csvData = [topic[1],topic[2],objectNameList[x],AVObjectId]
			csvWrite(csvData,objectType)
	else :
		for x in range(numberOfObject) :
			AOObjectId = AOObjectId + 1
			objectList.append(AOObjectId)
			csvData = [topic[1],topic[2],objectNameList[x],AOObjectId,cmdId]
			csvWrite(csvData,objectType)
	print("csvData")
	#BACnetDataWrite(objectId,presentValue[x])
	return objectList

def csvWrite(csvData, objectType):
	""" Write the information in csv log"""

	#objectPrefix = "temp_"
	print("csv write")

	if objectType == ANALOG_VALUE :
		f = open("config.csv", "a", newline='')
	else :
		f = open("analogOutputRegistration.csv", "a", newline='')
	writer = csv.writer(f)
	writer.writerow(csvData)
	print(csvData)
	f.close()
